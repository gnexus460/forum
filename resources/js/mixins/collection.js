export default {

    data() {

        return {

            items: []

        }

    },

    methods: {

        add(reply) {

            this.items.push(reply);

            this.$emit('added');

        },

        remove(index) {

            this.$emit('removed');

            this.items.splice(index, 1);

        }

    }

}