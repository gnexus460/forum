@component('profiles.activities.activity')
    @slot('heading')
        {{--{{ dd($activity->subject) }}--}}
        {{ $profileUser->name }} published a thread
        <a href="{{ $activity->subject->path() }}">{{$activity->subject->title }}</a>
    @endslot

    @slot('body')
        {{$activity->subject->body}}
    @endslot
@endcomponent