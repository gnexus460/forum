@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Forum threads</div>

                    <div class="card-body">
                       @forelse($threads as $thread)
                           <div class="level">
                               <article>
                                   <h4>
                                       <a href="{{ $thread->path() }}">
                                           @if(auth()->check() && $thread->hasUpdatesFor(auth()->user()))
                                               <strong>{{ $thread->title }}</strong>
                                           @else
                                               {{ $thread->title }}
                                           @endif
                                       </a>
                                   </h4>
                                   <div class="body">{{ $thread->body }}</div>
                               </article>

                               <a href="{{ $thread->path() }}">
                                   {{ $thread->replies_count }} {{ str_plural('reply', $thread->replies_count) }}
                               </a>
                           </div>

                           <hr />
                        @empty
                           <p>There are no relevant results at this time</p>
                       @endforelse
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
