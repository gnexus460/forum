<reply :attributes="{{ $reply }}"
       v-cloak
       update_route="{{ route('replies.update', $reply->id)}}"
       destroy_route="{{ route('replies.destroy', $reply->id)}}"
       inline-template>

</reply>