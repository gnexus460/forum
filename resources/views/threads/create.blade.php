@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Create a new thread</div>

                    <div class="card-body">
                        <form action="{{ route('threads.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="channel_id">Chose channel:</label>
                                <select name="channel_id" id="channel_id" class="form-control">
                                    @foreach ($channels as $channel)
                                        <option value="{{ $channel->id }}">{{ $channel->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="title">Title:</label>
                                <input type="text" name="title" class="form-control" id="title" placeholder="title" value="{{ old('title') }}">
                            </div>

                            <div class="form-group">
                                <label for="body">Body:</label>
                                <textarea type="body" name="body" class="form-control" id="body" placeholder="title">{{ old('body') }}</textarea>
                            </div>

                            <input type="submit" class="btn btn-success" value="Publish">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
