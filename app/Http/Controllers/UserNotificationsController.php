<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserNotificationsController extends Controller
{


    /**
     * UserNotificationsController constructor.
     */
    public function __construct()
    {

        $this->middleware('auth');

    }

    /**
     * @param User $user
     * @param $notificationId
     */
    public function destroy(User $user, $notificationId)
    {

        $user->notifications()->findOrFail($notificationId)->markAsRead();

    }

    /**
     * @return mixed
     */
    public function index()
    {
        return auth()->user()->unreadNotifications;
    }
}
