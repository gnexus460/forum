<?php

namespace App\Http\Controllers;

use App\Reply;
use App\Inspections\Spam;
use App\Thread;
use Exception;
use Illuminate\Http\Request;

class RepliesController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth', ['except' => 'index']);

    }

    public function index($channelId, Thread $thread)
    {

        return $thread->replies()->paginate(2);
        
    }

    /**
     * @param $channelId
     * @param Thread $thread
     * @param Spam $spam
     * @return \Illuminate\Database\Eloquent\Model|\Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store($channelId, Thread $thread, Spam $spam)

    {

        try {
            $this->validate(request(), [
                'body' => 'required'
            ]);

            $spam->detect(request('body'));


            $reply = $thread->addReply([

                'body' => request('body'),

                'user_id' => auth()->id(),
            ]);
        } catch (Exception $e) {
            return response('Sorry, your reply could not be saved at this time.', 422);
        }

//        if(request()->wantsJson()) {

            return $reply->load('owner');

//        }

//        return back()->with('flash', 'Your reply has been left');

    }

    public function destroy($reply_id)
    {
        $reply = Reply::find($reply_id);

        $this->authorize('update', $reply);

        $reply->delete();

        if(request()->expectsJson()) {
            return response(['status' => 'Reply deleted']);
        }

        return back();
    }

    /**
     * @param $id
     * @param Spam $spam
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update($id, Spam $spam)
    {

        $reply = Reply::find($id);

        try{
            $this->authorize('update', $reply);

            $spam->detect(request('body'));
        } catch (Exception $e) {
            return response('Sorry, your reply could not be saved at this time.', 422);
        }

        $reply->update(['body' => request('body')]);
    }

}
