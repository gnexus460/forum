<?php

namespace App\Inspections;


class Spam
{

    protected $inspections = [
        InvalidKeywords::class,
        KeyHeldDown::class
    ];

    /**
     * @param $body
     * @return bool
     * @throws \Exception
     */
    public function detect($body)
    {

        foreach ($this->inspections as $inspection) {
            if(is_array($body)) {
                foreach ($body as $item) {
                    app($inspection)->detect($item);
                }
            } else {
                app($inspection)->detect($body);
            }
        }

//        $this->detectInvalidKeywords($body);
//        $this->detectHeldDown($body);

        return false;
    }
}