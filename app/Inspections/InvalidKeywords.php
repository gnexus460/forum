<?php

namespace App\Inspections;

use Exception;

class InvalidKeywords {

    protected $keywods = [
        'Yahoo customer support'
    ];

    /**
     * @param $body
     * @throws Exception
     */
    public function detect($body)
    {
        foreach ($this->keywods as $keyword) {

            if(strpos($body, $keyword) !== false) {
                throw new Exception('Your reply contains spam');
            }

        }
    }

}