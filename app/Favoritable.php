<?php
/**
 * Created by PhpStorm.
 * User: sadmin
 * Date: 01.06.19
 * Time: 16:46
 */

namespace App;


trait Favoritable
{

    protected static function bootFavoritable()
    {

        static::deleting(function ($model) {

            $model->favorites()->get()->each(function ($favorite) {
                $favorite->delete();
            });

        });

    }

    public function favorites()
    {

        return $this->morphMany(Favorite::class, 'favorited');

    }

    public function favorite($userId)
    {
        $attributes = ['user_id' => $userId];

        if ($this->favoriteNotExist($attributes)) {

            $this->favorites()->create($attributes);

        }

    }



    public function unfavorite()
    {
        $attributes = ['user_id' => auth()->id()];

        $this->favorites()->where($attributes)->get()->each(function ($favorite){

            $favorite->delete();

        });
    }

    /**
     *
     * @return bool
     */
    public function favoriteNotExist($attributes): bool
    {
        return !$this->favorites()->where($attributes)->exists();
    }

    public function isFavorited()
    {

        return !!$this->favorites->where('user_id', auth()->id())->count();

    }

    public function getIsFavoritedAttribute()
    {
        return $this->isFavorited();
    }

    public function getFavoritesCountAttribute()
    {
        return $this->favorites->count();
    }
}