<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class ParticipateInForumTest extends TestCase
{

    use DatabaseMigrations;

    /** @test */

    function unauthenticated_user_cannot_add_replies()
    {

        $this->withExceptionHandling()
            ->post('threads/some-channel/1/replies', [])
            ->assertRedirect('login');

    }

    /** @test */

    function an_autenticated_user_may_participate_in_forum_threads()
    {

        $user = factory('App\User')->create();

        $this->be($user);

        $thread = factory('App\Thread')->create();

        $reply = factory('App\Reply')->make();

        $this->post($thread->path().'/replies', $reply->toArray());

        $this->assertDatabaseHas('replies', ['body' => $reply->body]);

        $this->assertEquals(1, $thread->fresh()->replies_count);

    }

    /** @test */

    function a_reply_requires_a_body()

    {

        $this->withExceptionHandling()->signIn();

        $thread = factory('App\Thread')->create();

        $reply = factory('App\Reply', ['body' => null])->make();

        $this->post($thread->path().'/replies', $reply->toArray())
            ->assertSessionHasErrors('body');

    }

    /** @test */
    
    function unauthorized_users_cannot_delete_replies()
    
    {

        $this->withExceptionHandling();

        $reply = create('App\Reply');

        $this->delete(route('replies.destroy', $reply->id))
            ->assertRedirect('login');

        $this->signIn();

        $this->delete(route('replies.destroy', $reply->id))
            ->assertStatus(403);
        
    }

    /** @test */

    function unauthorized_users_cannot_update_replies()

    {

        $this->withExceptionHandling();

        $reply = create('App\Reply');

        $this->patch(route('replies.destroy', $reply->id))
            ->assertRedirect('login');

        $this->signIn();

        $this->patch(route('replies.destroy', $reply->id))
            ->assertStatus(403);

    }
    
    /** @test */
    
    function authorized_users_can_delete_replies()
    
    {
        $this->signIn();

        $reply = create('App\Reply', ['user_id' => auth()->id()]);

        $this->delete(route('replies.destroy', $reply->id))->assertStatus(302);

        $this->assertDatabaseMissing('replies', ['id' => $reply->id]);


    }
    
    /** @test */
    
    function authorized_users_can_update_replies()
    
    {

        $this->signIn();

        $reply = create('App\Reply', ['user_id' => auth()->id()]);

        $this->patch(route('replies.update', $reply->id), ['body' => 'You been changed, fool']);

        $this->assertDatabaseHas('replies', ['id' => $reply->id, 'body' => 'You been changed, fool']);
        
    }
    
    /** @test */
    
    function replies_that_contain_spam_may_not_be_created()
    
    {
    
        $this->signIn();

        $thread = create('App\Thread');

        $reply = make('App\Reply', [
            'body' => 'Yahoo customer support'
        ]);

        $this->expectException(\Exception::class);

        $this->post($thread->path() . '/replies', $reply->toArray());
        
    }
}
