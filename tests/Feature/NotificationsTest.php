<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Notifications\DatabaseNotification;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NotificationsTest extends TestCase
{

    use DatabaseMigrations;

    public function setUp() : void
    {
        parent::setUp();

        $this->signIn();
    }

    /** @test */
    
    function a_notification_is_prepared_when_a_subscribed_thread_recieves_a_new_reply()
    
    {

        $thread = create('App\Thread')->subscribe();

        $this->assertCount(0, auth()->user()->notifications);

        $thread->addReply([

            'user_id' => auth ()->id(),

            'body' => 'some reply here'

        ]);

        $this->assertCount(0, auth()->user()->fresh()->notifications);

        $thread->addReply([

            'user_id' => create('App\User')->id,

            'body' => 'some reply here'

        ]);

        $this->assertCount(1, auth()->user()->fresh()->notifications);
        
    }

    /** @test */
    
    function a_user_can_fetch_their_unread_notifications()
    
    {

        create(DatabaseNotification::class);

        $response = $this->getJson("/profiles/". auth()->user()->name ."/notifications/")->json();

        $this->assertCount(1, $response);
        
    }
    
    /** @test */
    
    function a_user_can_mark_a_notification_as_read()
    
    {

        create(DatabaseNotification::class);

        $this->assertCount(1, auth()->user()->unreadNotifications);

        $notificationId = auth()->user()->unreadNotifications->first()->id;

        $this->delete("/profiles/". auth()->user()->name ."/notifications/{$notificationId}");

        $this->assertCount(0, auth()->user()->fresh()->unreadNotifications);


    }
}
